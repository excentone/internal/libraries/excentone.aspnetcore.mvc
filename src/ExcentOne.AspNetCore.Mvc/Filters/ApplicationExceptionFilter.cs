﻿using System.Dynamic;
using System.Net;
using ExcentOne.AspNetCore.Mvc.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace ExcentOne.AspNetCore.Mvc.Filters
{
    public class ApplicationExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;

            if (exception is ApplicationLevelException)
            {
                dynamic exObj = new ExpandoObject();
                exObj.ErrorMessage = exception.Message;

                context.Result = new ContentResult
                {
                    Content = JsonConvert.SerializeObject(exObj),
                    ContentType = "application/json",
                    StatusCode = (int?)HttpStatusCode.BadRequest
                };
            }
            else
            {
                context.Result = new ContentResult
                {
                    Content = JsonConvert.SerializeObject(exception),
                    ContentType = "application/json",
                    StatusCode = (int?)HttpStatusCode.BadRequest
                };
            }
            
        }
    }
}