﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ExcentOne.AspNetCore.Mvc.Extensions.Attributes
{
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class BaseActionFilterAttribute : ActionFilterAttribute
    {
        public static readonly string EXCENTONE_APPLICATION_HEADER_NAME = "X-ExcentOne-ApplicationKey";
        public static readonly string USER_INFO_HEADER_NAME = "X-ExcentOne-UserInfo";
        public static readonly string USER_FULLNAME_HEADER_NAME = "X-ExcentOne-UserFullName";

        private protected bool IsHttpHeaderValid(ActionExecutingContext filterContext, string headerName)
        {
            return filterContext.HttpContext.Request.Headers.ContainsKey(headerName);
        }
    }
}