﻿using System.Linq;
using ExcentOne.AspNetCore.Mvc.ActionResults;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;

namespace ExcentOne.AspNetCore.Mvc.Extensions.Attributes
{
    public class RequireUserFullNameAttribute : BaseActionFilterAttribute
    {
        public RequireUserFullNameAttribute(IWebHostEnvironment env)
        {
            Environment = env;
        }

        public IWebHostEnvironment Environment { get; }


        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (filterContext.ActionDescriptor.FilterDescriptors.Any(filter =>
                filter.Filter.GetType() == typeof(SkipRequireUserFullNameAttribute))) return;

            if (Environment != null && Environment.IsDevelopment()) return;
            if (!IsHttpHeaderValid(filterContext, USER_FULLNAME_HEADER_NAME))
                filterContext.Result = new InvalidUserFullNameActionResult();
        }
    }

    public class SkipRequireUserFullNameAttribute : BaseActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }
    }
}