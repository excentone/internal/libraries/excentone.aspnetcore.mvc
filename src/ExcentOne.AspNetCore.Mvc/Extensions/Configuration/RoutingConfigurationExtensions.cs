﻿using ExcentOne.AspNetCore.Mvc.Routing.Transformers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.Extensions.DependencyInjection;

namespace ExcentOne.AspNetCore.Mvc.Extensions.Configuration
{
    public static class RoutingConfigurationExtensions
    {
        public static MvcOptions SlugifyRouteTokens(this MvcOptions mvcOptions, bool slugify = true)
        {
            if (slugify)
            {
                mvcOptions
                    .Conventions
                    .Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
            }

            return mvcOptions;
        }
    }
}
