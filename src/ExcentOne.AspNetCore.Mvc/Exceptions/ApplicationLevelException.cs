﻿using System;

namespace ExcentOne.AspNetCore.Mvc.Exceptions
{
    public class ApplicationLevelException : Exception
    {
        public ApplicationLevelException()
        {
        }

        public ApplicationLevelException(string message)
            : base(message)
        {
        }

        public ApplicationLevelException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}