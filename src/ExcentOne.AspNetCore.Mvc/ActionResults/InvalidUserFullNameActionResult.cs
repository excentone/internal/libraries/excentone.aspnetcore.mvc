﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ExcentOne.AspNetCore.Mvc.ActionResults
{
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public class InvalidUserFullNameActionResult : IActionResult
    {
        public static string HTTPHEADER_INVALID_USER_FULLNAME = "User Full Name is undefined";
        public static int HTTPCODE_INVALID_USER_FULLNAME = 903;

        public async Task ExecuteResultAsync(ActionContext context)
        {
            var objectResult = new ObjectResult(new ArgumentException(HTTPHEADER_INVALID_USER_FULLNAME))
            {
                StatusCode = HTTPCODE_INVALID_USER_FULLNAME
            };

            await objectResult.ExecuteResultAsync(context);
        }
    }
}