﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ExcentOne.AspNetCore.Mvc.ActionResults
{
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public class InvalidUserInfoActionResult : IActionResult
    {
        public static string HTTPHEADER_INVALID_USER_INFO = "User Information is undefined";
        public static int HTTPCODE_INVALID_USER_INFO = 901;

        public async Task ExecuteResultAsync(ActionContext context)
        {
            var objectResult = new ObjectResult(new ArgumentException(HTTPHEADER_INVALID_USER_INFO))
            {
                StatusCode = HTTPCODE_INVALID_USER_INFO
            };

            await objectResult.ExecuteResultAsync(context);
        }
    }
}