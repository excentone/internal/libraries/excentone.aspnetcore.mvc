﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ExcentOne.AspNetCore.Mvc.ActionResults
{
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public class NullApplicationTokenActionResult : IActionResult
    {
        public static string HTTPHEADER_NULL_APPLICATION_TOKEN = "Application Token cannot be null";
        public static int HTTPCODE_NULL_APPLICATION_TOKEN = 900;
        

        public async Task ExecuteResultAsync(ActionContext context)
        {
            var objectResult = new ObjectResult(new ArgumentException(HTTPHEADER_NULL_APPLICATION_TOKEN))
            {
                StatusCode = HTTPCODE_NULL_APPLICATION_TOKEN
            };

            await objectResult.ExecuteResultAsync(context);
        }
    }
}